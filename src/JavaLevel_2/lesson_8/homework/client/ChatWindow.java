package JavaLevel_2.lesson_8.homework.client;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ChatWindow extends JFrame {

    private JTextArea chatHistory;
    private JTextArea clientsTextarea;
    private JTextField message;
    private JPanel top;
    private JPanel bottom;
    private JTextField login;
    private JPasswordField pass;
    private ClientConnection clientConnection;

    public ChatWindow() {

        setTitle("Чат-клиент");
        setSize(400, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        chatHistory = new JTextArea();
        chatHistory.setLineWrap(true);
        chatHistory.setEditable(false);
        JScrollPane jScrollPane = new JScrollPane(chatHistory);

        bottom = new JPanel();
        bottom.setLayout(new BorderLayout());
        bottom.setPreferredSize(new Dimension(300, 50));

        JButton send = new JButton("Send");
        message = new JTextField();
        message.setPreferredSize(new Dimension(200, 50));

        login = new JTextField();
        pass = new JPasswordField();
        JButton auth = new JButton("Login");
        top = new JPanel(new GridLayout(1, 3));
        top.add(login);
        top.add(pass);
        top.add(auth);

        JPanel clientsPanel = new JPanel();
        clientsPanel.setLayout(new BoxLayout(clientsPanel, BoxLayout.Y_AXIS));
        clientsPanel.setBorder(new EmptyBorder(10,0,0,0));
        JLabel clientsLabel = new JLabel("Участники чата:");
        clientsTextarea = new JTextArea();
        clientsTextarea.setEditable(false);
        clientsTextarea.setLineWrap(true);
        JScrollPane clientsScrollpane = new JScrollPane(clientsTextarea);
        clientsPanel.add(clientsLabel);
        clientsPanel.add(clientsScrollpane);


        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMessage();
            }
        });
        message.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMessage();
            }
        });
        auth.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                auth();
            }
        });

        bottom.add(send, BorderLayout.EAST);
        bottom.add(message, BorderLayout.CENTER);

        add(jScrollPane, BorderLayout.CENTER);
        add(bottom, BorderLayout.SOUTH);
        add(top, BorderLayout.NORTH);
        add(clientsPanel, BorderLayout.EAST);

        switchWindows();
        setVisible(true);
    }

    public void setClientsList(String[] clients) {
        clientsTextarea.setText("");
        for (String client : clients) {
            clientsTextarea.append(client + "\n");
        }
    }

    public void sendMessage() {
        String msg = this.message.getText();
        this.message.setText("");
        clientConnection.sendMessage(msg);
    }

    // Send login and pass to server
    public void auth() {
        if (clientConnection == null)
        {
            clientConnection = new ClientConnection();
            clientConnection.init(this);
        }
        else if (clientConnection.isSocketClose())
            clientConnection.init(this);

        clientConnection.auth(login.getText(), new String(pass.getPassword()));
        login.setText("");
        pass.setText("");
    }

    // show input message
    public void showMessage(String message) {
        chatHistory.append(message + "\n");
        chatHistory.setCaretPosition(chatHistory.getDocument().getLength());
    }

    // switch top and bottom panel depending on authorize
    public void switchWindows() {
        if (clientConnection != null)
        {
            top.setVisible(!clientConnection.isAuthorized());
            bottom.setVisible(clientConnection.isAuthorized());
        }
        else
        {
            top.setVisible(true);
            bottom.setVisible(false);
        }
    }

}
