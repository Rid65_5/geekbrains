package JavaLevel_2.lesson_8.homework.client;

import JavaLevel_2.lesson_8.homework.common.ServerConst;
import JavaLevel_2.lesson_8.homework.common.Server_API;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class ClientConnection implements Server_API, ServerConst {

    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;
    private boolean isAuthorized = false;
    private String name;
    private ChatWindow view;
    private long dateConnect;

    public boolean isSocketClose() {
        return socket.isClosed();
    }

    public boolean isAuthorized() {
        return  isAuthorized;
    }

    public void setAuthorized(boolean authorized) {
        this.isAuthorized = authorized;
    }

    public void init(ChatWindow view) {
        try {
            this.dateConnect = System.currentTimeMillis();
            this.view = view;
            this.socket = new Socket(SERVER_URL, PORT);
            this.out = new DataOutputStream(socket.getOutputStream());
            this.in = new DataInputStream(socket.getInputStream());

            // thread calculate number of seconds
            Thread t1 = new Thread(() -> {
                try {
                    this.dateConnect = System.currentTimeMillis();

                    while (true)
                    {
                        if (!this.isAuthorized())
                        {
                            // if the user has't authorized within 5 sec.
                            long diff = System.currentTimeMillis() - this.dateConnect;
                            if (diff > TIME_CLOSE_SOCKET)
                            {
                                view.showMessage("Time is over. You disconnected.");
                                throw new InterruptedException();
                            }
                        }
                        else
                            break;

                        Thread.sleep(1000);

                    }
                } catch (InterruptedException e) {
                    System.out.println("Client was disconnected!");
                    disconnect();
                }
            });
            t1.start();

            Thread t2 = new Thread(() -> {
                try {

                    // wait message about success authorize
                    while (true)
                    {
                        if (!this.socket.isClosed())
                        {
                            String message = in.readUTF();
                            if (message.startsWith(AUTH_SUCCESSFULL))
                            {
                                setAuthorized(true);
                                name = message.split(" ")[1];
                                view.switchWindows();
                                break;
                            }
                            view.showMessage(message);
                        }
                    }

                    // wait message
                    while (true)
                    {
                        if (!this.socket.isClosed())
                        {
                            String message = in.readUTF();
                            if (message.startsWith(SYSTEM_SYMBOL))
                            {
                                String[] elements = message.split(" ");
                                if (elements[0].equals(CLOSE_CONNECTION))
                                {
                                    setAuthorized(false);
                                    view.showMessage(message.substring(CLOSE_CONNECTION.length() + 1));
                                    view.switchWindows();
                                }
                                else if (elements[0].equals(CLIENTS_LIST))
                                {
                                    String[] clientsList = new String[elements.length - 1];
                                    System.arraycopy(elements, 1, clientsList, 0, elements.length - 1);
                                    view.setClientsList(clientsList);
                                }
                            }
                            else
                            {
                                view.showMessage(message);
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    disconnect();
                }
            });
            t2.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String msg) {
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void auth(String login, String pass) {
        if (socket.isClosed() || socket == null)
            init(this.view);

        try {
            out.writeUTF(AUTH + " " + login + " " + pass);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            out.writeUTF(CLOSE_CONNECTION);
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
