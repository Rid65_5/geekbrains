package JavaLevel_2.lesson_8.homework.common;

public interface ServerConst {
    int PORT = 8189;
    String SERVER_URL = "localhost";
    int TIME_CLOSE_SOCKET = 10000;
}
