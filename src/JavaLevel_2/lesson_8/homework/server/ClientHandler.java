package JavaLevel_2.lesson_8.homework.server;

import JavaLevel_2.lesson_8.homework.common.Server_API;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler implements Server_API {

    private Server server;
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;
    private String name;

    public String getName() {
        return name;
    }

    public ClientHandler(Server server, Socket socket) {
        try {
            this.server = server;
            this.socket = socket;
            this.in = new DataInputStream(socket.getInputStream());
            this.out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        new Thread(() -> {

            try {
                // Auth
                while (true)
                {
                    String message = in.readUTF();
                    if (message.startsWith(AUTH))
                    {
                        String name = null;
                        String[] elements = message.split(" ");
                        if (elements.length >= 3) {
                            name = server.getAuthService().getNickByLoginPass(elements[1], elements[2]);
                        } else
                            name = null;

                        if (name != null)
                        {
                            if (!server.isNickBusy(name))
                            {
                                sendMessage(AUTH_SUCCESSFULL + " " + name);
                                this.name = name;
                                server.broadcast(name + " has entered the chat room");
                                server.broadcastClientList();
                                break;
                            }
                            else
                                sendMessage("Login is busy");
                        }
                        else
                            sendMessage("Wrong login/pass!");
                    }
                    else if (message.equalsIgnoreCase(CLOSE_CONNECTION))
                    {
                        throw new InterruptedException();
                    }
                    else
                        sendMessage("You should authorize first!");
                }

                // Input message
                while (true)
                {
                    String message = in.readUTF();
                    if (message.startsWith(SYSTEM_SYMBOL))
                    {
                        if (message.equalsIgnoreCase(CLOSE_CONNECTION))
                            break;
                        else if (message.startsWith(PRIVATE_MESSAGE_FLAG)) {
                            String[] elements = message.split(" ");
                            String beginstr = elements[0] + " " + elements[1] + " ";
                            server.sendPrivateMessage(elements[1], name + ": " + message.substring(beginstr.length()), this);
                        }
                        else
                            sendMessage("Command doesn't exist!");
                    }
                    else
                    {
                        System.out.println("client: " + message);
                        server.broadcast(name + ": " + message);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (InterruptedException e) {
                System.out.println("Client was disconnected.");;
            }
            finally {
                disconnect();
            }

        }).start();
    }

    public void sendMessage(String msg) {
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        sendMessage(CLOSE_CONNECTION + " You have been disconnected!");
        server.unsubscribeMe(this);
        server.broadcastClientList();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
