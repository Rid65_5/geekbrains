package JavaLevel_2.lesson_8.homework.server;

import java.util.ArrayList;

public class BaseAuthService implements AuthService {

    private class Entry {
       private String login;
       private String pass;
       private String name;

        public Entry(String login, String pass, String name) {
            this.login = login;
            this.pass = pass;
            this.name = name;
        }
    }

    private ArrayList<Entry> entries;

    public BaseAuthService() {
        this.entries = new ArrayList<>();
        this.entries.add(new Entry("login1", "pass1", "Rick"));
        this.entries.add(new Entry("login2", "pass2", "Tom"));
        this.entries.add(new Entry("login3", "pass3", "Sara"));
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public String getNickByLoginPass(String login, String pass) {
        if (login != null && pass != null)
        {
            for (Entry entry : entries)
            {
                if (entry.login.equals(login) && entry.pass.equals(pass))
                    return entry.name;
            }
        }
        return null;
    }
}
