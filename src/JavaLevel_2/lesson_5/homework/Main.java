package JavaLevel_2.lesson_5.homework;

import java.util.Arrays;

public class Main {

    static final int size = 10000000;
    static final int h = size/2;

    public static void main(String[] args) {
        method1();
        method2();
    }

    public static void method1() {
        float[] arr = new float[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 1;
        }

        long a = System.currentTimeMillis();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (float)(arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
        }

        long time = System.currentTimeMillis() - a;

        System.out.println("Время выполнения первого метода: " + time);
    }

    public static void method2() {

        // заполняем массив единицами
        float[] arr = new float[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 1;
        }

        long a = System.currentTimeMillis();

        // разбиваем массив
        float[] arr1 = new float[h];
        float[] arr2 = new float[h];
        System.arraycopy(arr, 0, arr1, 0, h);
        System.arraycopy(arr, h, arr2, 0, h);

        // создаем потоки
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                calcArray(arr1, 0);
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                calcArray(arr2, h);
            }
        });

        // запуск потоков
        try {
            t1.start();
            t2.start();
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // склейка массивов
        System.arraycopy(arr1, 0, arr, 0, h);
        System.arraycopy(arr2, 0, arr, h, h);

        long time = System.currentTimeMillis() - a;

        System.out.println("Время выполнения второго метода: " + time);
    }

    // метод выполняет вычисления с помощью итератора
    private static void calcArray(float[] arr, int iterator) {
        for (int i = 0; i < arr.length; i++) {
                arr[i] = (float)(arr[i] * Math.sin(0.2f + iterator / 5) * Math.cos(0.2f + iterator / 5) * Math.cos(0.4f + iterator / 2));
                iterator++;
        }
    }

}
