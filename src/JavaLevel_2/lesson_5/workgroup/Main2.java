package JavaLevel_2.lesson_5.workgroup;

public class Main2 {
    public static void main(String[] args){
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run(){
                int time = 0;
                while(true){
                    try{
                        Thread.sleep(1000);
                        time++;
                        System.out.println("Time: " + time);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        });
        t1.setPriority(10); // от 1 до 10
        t1.setDaemon(true); //маркер, что поток не важный
        t1.start();
        try{
            Thread.sleep(12000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        t1.interrupt(); //завершение потока
        System.out.println("End!"); //main - это тоже поток
    }
}
