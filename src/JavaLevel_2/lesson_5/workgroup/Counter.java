package JavaLevel_2.lesson_5.workgroup;

public class Counter {
    private int x;
    public Counter(){
        x = 0;
    }
    public int getX(){
        return x;
    }
    public void setX(int x){
        this.x = x;
    }
    public synchronized void inc(){ //только один синхронизированный метод может выполняться за 1 раз 1 потоком
        this.x++;
    }
    public synchronized void dec(){
        this.x--;
    }
}
