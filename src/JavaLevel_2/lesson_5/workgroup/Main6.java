package JavaLevel_2.lesson_5.workgroup;

public class Main6 {
    public static void main(String[] args){
        ATM atm = new ATM(100);
        User u1 = new User("Vasya");
        User u2 = new User("Kolya");
        User u3 = new User("Vova");
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run(){
                atm.withdraw(50, u1);
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run(){
                atm.withdraw(50, u2);
            }
        });
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run(){
                atm.withdraw(50, u3);
            }
        });
        try{
            t1.start();
            t2.start();
            t3.start();
            t1.join();
            t2.join();
            t3.join();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        atm.info();
    }

}
