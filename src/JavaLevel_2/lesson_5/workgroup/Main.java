package JavaLevel_2.lesson_5.workgroup;

public class Main {
    public static void main(String[] args){
        MyThread myThread1 = new MyThread();
        MyThread myThread2 = new MyThread();

        myThread1.start(); //запускает поток
        // myThread1.run(); – это просто вызов метода
        myThread2.start();
        Thread thread1 = new Thread(new MyRunnable());
        thread1.start();
    }
}
