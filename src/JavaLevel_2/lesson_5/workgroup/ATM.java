package JavaLevel_2.lesson_5.workgroup;

public class ATM {
    private int money;
    public ATM(int money){
        this.money = money;
    }
    public synchronized void withdraw(int amount, User u){
        if(this.money >= amount){
            try{
                Thread.sleep(2);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            money-= amount;
            System.out.println("Снято: " + amount + " человвеком " + u.getName());
        }else System.out.println("Человеку " + u.getName() + " не хватило денег!");
    }
    public void info(){
        System.out.println("ATM account: " + this.money);
    }
}
