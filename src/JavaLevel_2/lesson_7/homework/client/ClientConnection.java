package JavaLevel_2.lesson_7.homework.client;

import JavaLevel_2.lesson_7.homework.common.ServerConst;
import JavaLevel_2.lesson_7.homework.common.Server_API;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientConnection implements Server_API, ServerConst {

    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;
    private boolean isAuthorized = false;

    public boolean isAuthorized() { return  isAuthorized; }

    public void setAuthorized(boolean authorized) { this.isAuthorized = authorized; }

    public void init(ChatWindow view) {
        try {

            this.socket = new Socket(SERVER_URL, PORT);
            this.out = new DataOutputStream(socket.getOutputStream());
            this.in = new DataInputStream(socket.getInputStream());

            new Thread(() -> {
                try {
                    while (true)
                    {
                        String message = in.readUTF();
                        if (message.startsWith(AUTH_SUCCESSFULL))
                        {
                            setAuthorized(true);
                            view.switchWindows();
                            break;
                        }
                        view.showMessage(message);
                    }

                    while (true)
                    {
                        String message = in.readUTF();
                        if (message.startsWith(SYSTEM_SYMBOL))
                        {
                            String[] elements = message.split(" ");
                            if (elements[0].equals(CLOSE_CONNECTION))
                            {
                                setAuthorized(false);
                                view.showMessage(message.substring(CLOSE_CONNECTION.length() + 1));
                                view.switchWindows();
                            }
                        }
                        else
                        {
                            view.showMessage(message);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String msg) {
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void auth(String login, String pass) {
        try {
            out.writeUTF(AUTH + " " + login + " " + pass);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            out.writeUTF(CLOSE_CONNECTION);
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
