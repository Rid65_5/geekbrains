package JavaLevel_2.lesson_7.homework.client;

import JavaLevel_2.lesson_7.homework.client.ClientConnection;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChatWindow extends JFrame {

    private JTextArea chatHistory;
    private JTextField message;
    private JPanel top;
    private JPanel bottom;
    private JTextField login;
    private JPasswordField pass;
    private ClientConnection clientConnection;

    public ChatWindow() {

        clientConnection = new ClientConnection();
        clientConnection.init(this);

        setTitle("Чат-клиент");
        setSize(400, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        chatHistory = new JTextArea();
        chatHistory.setLineWrap(true);
        chatHistory.setEditable(false);
        JScrollPane jScrollPane = new JScrollPane(chatHistory);

        bottom = new JPanel();
        bottom.setLayout(new BorderLayout());
        bottom.setPreferredSize(new Dimension(300, 50));

        JButton send = new JButton("Send");
        message = new JTextField();
        message.setPreferredSize(new Dimension(200, 50));

        login = new JTextField();
        pass = new JPasswordField();
        JButton auth = new JButton("Login");
        top = new JPanel(new GridLayout(1, 3));
        top.add(login);
        top.add(pass);
        top.add(auth);

        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMessage();
            }
        });
        message.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMessage();
            }
        });
        auth.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                auth();
            }
        });

        bottom.add(send, BorderLayout.EAST);
        bottom.add(message, BorderLayout.CENTER);

        add(jScrollPane, BorderLayout.CENTER);
        add(bottom, BorderLayout.SOUTH);
        add(top, BorderLayout.NORTH);

        switchWindows();
        setVisible(true);
    }

    public void sendMessage() {
        String msg = this.message.getText();
        this.message.setText("");
        clientConnection.sendMessage(msg);
    }

    // Send login and pass to server
    public void auth() {
        clientConnection.auth(login.getText(), new String(pass.getPassword()));
        login.setText("");
        pass.setText("");
    }

    // show input message
    public void showMessage(String message) {
        chatHistory.append(message + "\n");
        chatHistory.setCaretPosition(chatHistory.getDocument().getLength());
    }

    // switch top and bottom panel depending on authorize
    public void switchWindows() {
        top.setVisible(!clientConnection.isAuthorized());
        bottom.setVisible(clientConnection.isAuthorized());
    }

}
