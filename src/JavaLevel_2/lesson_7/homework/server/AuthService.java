package JavaLevel_2.lesson_7.homework.server;

public interface AuthService {

    void start();

    void stop();

    String getNickByLoginPass(String login, String pass);

}
