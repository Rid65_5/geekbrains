package JavaLevel_2.lesson_7.homework.server;

import JavaLevel_2.lesson_7.homework.common.ServerConst;
import JavaLevel_2.lesson_7.homework.server.ClientHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class Server implements ServerConst {

    private Vector<ClientHandler> clients;
    private AuthService authService;

    public AuthService getAuthService() {
        return authService;
    }

    public Server() {
        ServerSocket serverSocket = null;
        Socket socket = null;
        clients = new Vector<>();

        try {
            serverSocket = new ServerSocket(PORT);
            authService = new BaseAuthService();
            authService.start();
            System.out.println("Сервер запущен, ждем подключений...");
            while (true)
            {
                socket = serverSocket.accept();
                subscribe(new ClientHandler(this, socket));
                System.out.println("Клиент подключился...");

            }
        } catch (IOException e) {
            System.out.println("Ошибка инициализации!");
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized boolean isNickBusy(String nick) {
        for (ClientHandler client : clients) {
            if (client.getName().equals(nick)) return true;
        }
        return false;
    }

    public void broadcast(String msg) {
        for (ClientHandler client : clients) {
            client.sendMessage(msg);
        }
    }

    public void sendPrivateMessage(String name, String msg, ClientHandler sender) {
        ClientHandler client = null;
        if ((client = getClientByName(name)) != null)
        {
            client.sendMessage("(private) " + msg);
            sender.sendMessage("(private)" + msg);
        }
        else
        {
            String report = "Send a private message to failed! User \"" + name + "\" doesn't exist.";
            sender.sendMessage(report);
        }
    }

    public void unsubscribeMe(ClientHandler c) {
        clients.remove(c);
    }

    public void subscribe(ClientHandler c) {
        clients.add(c);
    }

    public ClientHandler getClientByName(String name) {
        for (ClientHandler client : clients) {
            if (client.getName().equals(name))
                return client;
        }
        return null;
    }

    public boolean clientExistsByName(String name) {
        for (ClientHandler client : clients) {
            if (client.getName().equals(name))
                return true;
        }
        return false;
    }
}
