package JavaLevel_2.lesson_7.homework.common;

public interface ServerConst {
    int PORT = 8189;
    String SERVER_URL = "localhost";
}
