package JavaLevel_2.lesson_7.homework.common;

public interface Server_API {
    String SYSTEM_SYMBOL = "/";
    String CLOSE_CONNECTION = "/end";
    String AUTH = "/auth";
    String AUTH_SUCCESSFULL = "/authok";
    String PRIVATE_MESSAGE_FLAG = "/w";
}
