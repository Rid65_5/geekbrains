package JavaLevel_2.lesson_1.obstacles;

import JavaLevel_2.lesson_1.participants.Animal;
import JavaLevel_2.lesson_1.participants.iChallengable;

public abstract class Obstacle {

    public abstract void doIt(iChallengable a);

}
