package JavaLevel_2.lesson_1.obstacles;

import JavaLevel_2.lesson_1.participants.*;

public class Pool extends Obstacle {

    private int length;

    public Pool(int length)
    {
        this.length = length;
    }

    @Override
    public void doIt(iChallengable a)
    {
        a.swim(this.length);
    }

}
