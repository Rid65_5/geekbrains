package JavaLevel_2.lesson_1.obstacles;

import JavaLevel_2.lesson_1.participants.*;

public class Wall extends Obstacle {

    private int height;

    public Wall(int height)
    {
        this.height = height;
    }

    @Override
    public void doIt(iChallengable a)
    {
        a.jump(this.height);
    }
}
