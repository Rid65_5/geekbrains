package JavaLevel_2.lesson_1.obstacles;

import JavaLevel_2.lesson_1.participants.*;

public class Cross extends Obstacle {

    private int length;

    public Cross(int length)
    {
        this.length = length;
    }

    @Override
    public void doIt(iChallengable a) {
        a.run(this.length);
    }
}
