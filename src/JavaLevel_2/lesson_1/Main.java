package JavaLevel_2.lesson_1;

import JavaLevel_2.lesson_1.obstacles.*;
import JavaLevel_2.lesson_1.participants.*;

public class Main {

    public static void main(String[] args) {

        //-- спортсмены
        iChallengable[] participants = {new Cat(1000, 5, 0, "Barsik"),
            new Dog(2000,2,500,"Sharik"),
            new Human(10000, 3, 1000, "Andry")
        };

        //-- упражнения
        Obstacle[] obstacles = {new Cross(900), new Wall(3), new Pool(100)};

        Team team = new Team("Team 1", participants); // Создаем команду
        Course course = new Course(obstacles); // Создаем полосу препятствий
        course.doCourse(team); // Команда выполняет упражнения из полосы препятствий

        System.out.println("Finished!\n");

        System.out.println("Информация о членах команды, успешно прошедших дистанцию:");
        team.printInfoSuccess();
        System.out.println("\nИнформация обо всех ччленах команды:");
        team.printInfo();


    }

}
