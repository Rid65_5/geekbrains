package JavaLevel_2.lesson_1;

import JavaLevel_2.lesson_1.obstacles.Obstacle;
import JavaLevel_2.lesson_1.participants.iChallengable;

public class Course {

    private Obstacle[] obstacles; // массив препятствий

    public Course(Obstacle[] obstacles) {
        this.obstacles = obstacles;
    }

    public void doCourse(Team team) {
        iChallengable[] teamMembers = team.getTeamMembers();
        for (int i = 0; i < obstacles.length; i++)
        {
            for (int j = 0; j < teamMembers.length; j++)
            {
                obstacles[i].doIt(teamMembers[j]); // каждый участник команды выполняет каждое препятствие из массива
            }
        }
    }
}
