package JavaLevel_2.lesson_1;

import JavaLevel_2.lesson_1.participants.*;

public class Team {

    private iChallengable[] teamMembers;
    private String teamName;

    public Team(String teamName, iChallengable[] teamMembers) {
        this.teamMembers = teamMembers;
        this.teamName = teamName;
    }

    public iChallengable[] getTeamMembers() {
        return teamMembers;
    }

    /*@Override
    public void run(int distance) {
        for (int i = 0; i < this.teamMembers.length; i++)
        {
            this.teamMembers[i].run(distance);
        }
    }

    @Override
    public void swim(int distance) {
        for (int i = 0; i < this.teamMembers.length; i++)
        {
            this.teamMembers[i].swim(distance);
        }
    }

    @Override
    public void jump(int height) {
        for (int i = 0; i < this.teamMembers.length; i++)
        {
            this.teamMembers[i].jump(height);
        }
    }*/

    //-- Информация обо всех членах команды
    public void printInfo() {
        for (int i = 0; i < this.teamMembers.length; i++)
        {
            this.teamMembers[i].printInfo();
        }
    }

    //-- Информация о членах команды, прошедших дистанцию
    public void printInfoSuccess() {
        for (int i = 0; i < this.teamMembers.length; i++)
        {
            if (this.teamMembers[i].isOnDistance())
                this.teamMembers[i].printInfo();
        }
    }

}
