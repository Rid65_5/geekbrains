package JavaLevel_2.lesson_1.participants;

public abstract class Animal extends Participant {

    public Animal(int maxRunDistance, int maxJumpHeight, int maxSwimDistance, String name) {
        super(maxRunDistance, maxJumpHeight, maxSwimDistance, name);
    }
}
