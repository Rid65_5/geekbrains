package JavaLevel_2.lesson_1.participants;

public class Cat extends Animal {

    public Cat(int maxRunDistance, int maxJumpHeight, int maxSwimDistance, String name) {
        super(maxRunDistance, maxJumpHeight, maxSwimDistance, name);
    }

}
