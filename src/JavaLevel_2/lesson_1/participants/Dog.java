package JavaLevel_2.lesson_1.participants;

public class Dog extends Animal {

    public Dog(int maxRunDistance, int maxJumpHeight, int maxSwimDistance, String name) {
        super(maxRunDistance, maxJumpHeight, maxSwimDistance, name);
    }
}
