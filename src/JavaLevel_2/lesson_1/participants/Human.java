package JavaLevel_2.lesson_1.participants;

public class Human extends Participant {

    public Human(int maxRunDistance, int maxJumpHeight, int maxSwimDistance, String name) {
        super(maxRunDistance, maxJumpHeight, maxSwimDistance, name);
    }
}
