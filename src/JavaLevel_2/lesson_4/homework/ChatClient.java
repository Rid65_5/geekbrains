package JavaLevel_2.lesson_4.homework;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChatClient extends JFrame {

    private long number = 1;

    public static void main(String[] args) {

        ChatClient chat = new ChatClient();

    }

    public ChatClient() {

        setTitle("Чат-клиент");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400, 630);
        setMinimumSize(new Dimension(400, 630));
        setMaximumSize(new Dimension(900, 700));
        setLocationRelativeTo(null);

        // Создаем внутреннюю панель
        JPanel panel = new JPanel();
        panel.setBackground(new Color(0,193,255, 30));
        panel.setPreferredSize(new Dimension(400, 650));
        panel.setBorder(new EmptyBorder(10,20,10,20));

        // Создаем textarea и скролбар
        JTextArea textArea = new JTextArea();
        textArea.setLineWrap(true);
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setPreferredSize(new Dimension(400, 500));
        scrollPane.setMinimumSize(new Dimension(400, 500));

        // создаем textfield
        JTextField textField = new JTextField("Введите комментарий..");
        textField.setPreferredSize(new Dimension(200, 30));
        textField.setMinimumSize(new Dimension(200, 30));

        // создаем button
        JButton button = new JButton("Send");
        button.setPreferredSize(new Dimension(70, 30));
        button.setMinimumSize(new Dimension(70, 30));

        // Испльзуем GridBagLayout
        GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.NORTH;
        c.gridheight = 1;
        c.gridwidth = 2;
        c.insets = new Insets(0,0,10,0);

        panel.add(scrollPane, c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0,0,0,10);
        panel.add(textField, c);

        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 0.1;
        c.insets = new Insets(0,0,0,0);
        panel.add(button, c);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = textField.getText();
                textArea.append("Message " + number + ": " + text + "\n\n");
                number++;
                textField.setText("");
                textField.requestFocus();
            }
        });

        textField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = textField.getText();
                textArea.append("Message " + number + ": " + text + "\n\n");
                number++;
                textField.setText("");
                textField.requestFocus();
            }
        });

        add(panel);
        setVisible(true);
    }

}
