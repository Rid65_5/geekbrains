package JavaLevel_2.lesson_4.workgroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TestWindow extends JFrame{
    public TestWindow(){
        setTitle("TestWindow");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400, 400);
        setLocationRelativeTo(null);
        setResizable(false);
        //текстовая зона для отображения истории, окно ввода текста и кнопка отправки сообщения
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        JTextField jtf = new JTextField();
        jtf.setSize(100, 30);

        JPanel jPanel = new JPanel();
        JTextArea jTextArea1 = new JTextArea();
        JTextArea jTextArea2 = new JTextArea();
        JTextArea jTextArea3 = new JTextArea();
//        jTextArea1.setPreferredSize(new Dimension(100, 100));
        jTextArea2.setPreferredSize(new Dimension(100, 100));
//        jTextArea3.setPreferredSize(new Dimension(100, 100));
        jTextArea1.setEditable(false);
        jTextArea1.setLineWrap(true);
        jTextArea2.setLineWrap(true);
        jTextArea3.setLineWrap(true);

        JScrollPane scrollPane1 = new JScrollPane(jTextArea1);
        scrollPane1.setPreferredSize(new Dimension(100, 100));
        JScrollPane scrollPane2 = new JScrollPane(jTextArea3);
        scrollPane2.setPreferredSize(new Dimension(100, 100));

        jPanel.add(scrollPane1, BorderLayout.WEST);
        jPanel.add(jTextArea2, BorderLayout.CENTER);
        jPanel.add(scrollPane2, BorderLayout.EAST);

        jPanel.setPreferredSize(new Dimension(400, 150));

        jtf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                jTextArea1.append(jtf.getText() + "\n");
                jTextArea3.append(jtf.getText() + "\n");
                jTextArea1.setCaretPosition(jTextArea1.getDocument().getLength());
                jtf.setText(""); //сбрасываем текст в окне набора текста
            }
        });


        add(jtf);
        add(jPanel);

        setVisible(true);
    }
}
