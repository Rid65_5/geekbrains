package JavaLevel_2.lesson_4.workgroup;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyWindow extends JFrame{
    public MyWindow(){
        setTitle("MyWindow");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //остановка процесса
        setSize(400, 400); //размер окна
        setLocationRelativeTo(null);
        setResizable(false);
        JButton button1 = new JButton("Center");
        JButton button2 = new JButton("East");
        JButton button3 = new JButton("West");
        JButton button4 = new JButton("North");
        JButton button5 = new JButton("South");

        JPanel jpeast = new JPanel();
        jpeast.setLayout(new FlowLayout());
        jpeast.setPreferredSize(new Dimension(150,400));

        JButton button6 = new JButton("test1");
        JButton button7 = new JButton("test2");

        jpeast.add(button6);
        jpeast.add(button7);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                System.out.println("Button1 Pressed!");
            }
        });

        add(button1, BorderLayout.CENTER);
        add(jpeast, BorderLayout.EAST);
        add(button3, BorderLayout.WEST);
        add(button4, BorderLayout.NORTH);
        add(button5, BorderLayout.SOUTH);

        setVisible(true); //вызывается последним
    }
}
