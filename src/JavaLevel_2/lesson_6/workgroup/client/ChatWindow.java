package JavaLevel_2.lesson_6.workgroup.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ChatWindow extends JFrame{
    private JTextField message;
    private JTextArea chatHistory;

    private final int SERVER_PORT = 8189;
    private final String SERVER_ADDRESS = "localhost";
    private Socket socket;
    private Scanner in;
    private PrintWriter out;

    public ChatWindow(){
        try{
            socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream());
        }catch(IOException e){
            e.printStackTrace();
        }
        setTitle("Chat");
        setSize(400, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        chatHistory = new JTextArea();
        chatHistory.setLineWrap(true);
        chatHistory.setEditable(false);
        JScrollPane jScrollPane = new JScrollPane(chatHistory);

        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        jPanel.setPreferredSize(new Dimension(300, 50));

        JButton send = new JButton("Send");
        message = new JTextField();
        message.setPreferredSize(new Dimension(200, 50));

        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                sendMessage();
            }
        });
        message.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                sendMessage();
            }
        });

        jPanel.add(send, BorderLayout.EAST);
        jPanel.add(message, BorderLayout.CENTER);

        add(jScrollPane, BorderLayout.CENTER);
        add(jPanel, BorderLayout.SOUTH);

        new Thread(new Runnable() {
            @Override
            public void run(){
                while(true){
                    if(in.hasNext()){
                        String string = in.nextLine();
                        chatHistory.append(string + "\n");
                        chatHistory.setCaretPosition(chatHistory.getDocument().getLength());
                    }
                }
            }
        }).start();

        setVisible(true);
    }
    public void sendMessage(){
        out.println(message.getText());
        out.flush();
        message.setText("");
    }

}
