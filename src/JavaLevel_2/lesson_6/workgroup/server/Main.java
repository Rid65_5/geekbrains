package JavaLevel_2.lesson_6.workgroup.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        ServerSocket serverSocket = null;
        Socket socket = null;
        try{
            serverSocket = new ServerSocket(8189);
            System.out.println("Сервер запущен, ждем клиентов");
            socket = serverSocket.accept(); //ждем подключений, сервер становится на паузу
            System.out.println("Клиент подключился");
            Scanner inputStream = new Scanner(socket.getInputStream());
            PrintWriter outputStream = new PrintWriter(socket.getOutputStream());
            while(true){
                String string = inputStream.nextLine();
                System.out.println(string);
                outputStream.println("Echo " + string); //складываем в буфер
                outputStream.flush(); //очистить буфер и отправить данные
            }
        }catch(IOException e){
            System.out.println("Ошибка инициализации");
        }finally{
            try{
                serverSocket.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }
}
