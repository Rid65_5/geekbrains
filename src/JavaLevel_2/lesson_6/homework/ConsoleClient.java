package JavaLevel_2.lesson_6.homework;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ConsoleClient {

    private final String HOST = "localhost";
    private final int PORT = 4045;
    private Socket socket;
    private Scanner inputStream;
    private Scanner scannerConsole;
    private PrintWriter outputStream;

    public static void main(String[] args) {
        ConsoleClient client = new ConsoleClient();
        client.go();
    }

    private void go() {
        try {
            socket = new Socket(HOST, PORT);
            inputStream = new Scanner(socket.getInputStream());
            outputStream = new PrintWriter(socket.getOutputStream());
            scannerConsole = new Scanner(System.in);

            System.out.println("Соединение с сервером установлено...");

            createThreads();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
                inputStream.close();
                outputStream.close();
                scannerConsole.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createThreads() {

        Thread t1 = new Thread(new Runnable() { // Поток для принятия сообщений с сервера
            @Override
            public void run() {
                while (true)
                {
                    if (inputStream.hasNext())
                    {
                        String msg = inputStream.nextLine();
                        System.out.println(msg);
                    }
                }
            }
        });

        Thread t2 = new Thread(new Runnable() { // поток для отправки сообщений на сервер через консоль
            @Override
            public void run() {
                while (true)
                {
                    if (scannerConsole.hasNext())
                    {
                        String msgConsole = scannerConsole.nextLine();
                        if (msgConsole != "" && msgConsole.length() > 0)
                        {
                            outputStream.println("Клиент: " + msgConsole);
                            outputStream.flush();
                        }
                    }
                }
            }
        });

        try {
            t1.start();
            t2.start();
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
