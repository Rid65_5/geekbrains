package JavaLevel_2.lesson_6.homework;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ConsoleServer {

    private final int PORT = 4045;
    private ServerSocket serverSocket;
    private Socket socket;
    private Scanner inputStream;
    private Scanner scannerConsole;
    private PrintWriter outputStream;

    public static void main(String[] args) {
        ConsoleServer server = new ConsoleServer();
        server.go();
    }


    private void go() {
        try {
            serverSocket = new ServerSocket(PORT); // Создали серверный сокет
            System.out.println("Сервер запущен, ждем клиентов...");
            socket = serverSocket.accept(); // серверный сокет ожидает подключения клиента и переводит клиента на отдельный сокет
            System.out.println("Клиент подключился...");
            inputStream = new Scanner(socket.getInputStream()); // принимает входящий поток от клиента
            scannerConsole = new Scanner(System.in); // сканирует консоль
            outputStream = new PrintWriter(socket.getOutputStream());

            createThreads(); // Создаем потоки

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
                socket.close();
                inputStream.close();
                outputStream.close();
                scannerConsole.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createThreads() {

        Thread t1 = new Thread(new Runnable() { // Отдельный поток для принятия сообщений от клиента
            @Override
            public void run() {
                while (true)
                {
                    if (inputStream.hasNext())
                    {
                        String msg = inputStream.nextLine();
                        System.out.println(msg);
                    }
                }
            }
        });

        Thread t2 = new Thread(new Runnable() { // Отдельный поток для отправки сообщений клиенту через консоль
            @Override
            public void run() {
                System.out.println("Можете написать что-нибудь клиенту...");
                while (true)
                {
                    if (scannerConsole.hasNext())
                    {
                        String msgConsole = scannerConsole.nextLine();
                        if (msgConsole != "" && msgConsole.length() > 0)
                        {
                            outputStream.println("Сервер: " + msgConsole);
                            outputStream.flush();
                        }
                    }
                }
            }
        });

        try {
            t1.start();
            t2.start();
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
