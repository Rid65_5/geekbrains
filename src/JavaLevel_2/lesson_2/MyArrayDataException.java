package JavaLevel_2.lesson_2;

public class MyArrayDataException extends RuntimeException {

    private int i;
    private int j;
    private String str;

    public MyArrayDataException(int i, int j, String str) {
        this.i      = i;
        this.j      = j;
        this.str    = str;
    }

    @Override
    public String toString() {
        return "Злое исключение! Не удалось преобразовать строку '" + str + "' к типу int в ячейке массива [" + i + "][" + j + "].";
    }
}
