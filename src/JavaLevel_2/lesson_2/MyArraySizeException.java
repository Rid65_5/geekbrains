package JavaLevel_2.lesson_2;

public class MyArraySizeException extends Exception {

    public MyArraySizeException(String message) {
        super(message);
    }
}
