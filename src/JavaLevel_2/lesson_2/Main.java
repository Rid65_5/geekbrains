package JavaLevel_2.lesson_2;

public class Main {

    public static int dimension = 4;

    public static void main(String[] args) {

        String[][] array = {
                {"1", "2", "3", "4"},
                {"2", "2", "2", "2"},
                {"3", "3", "3", "3"},
                {"4", "4", "4", "4"}
        };

        try
        {
            int sum = sumArray(array);
            System.out.println("Сумма элементов массива: " + sum);
        }
        catch (MyArraySizeException ex)
        {
            ex.printStackTrace();
        }
        catch (MyArrayDataException ex)
        {
            ex.printStackTrace();
        }

    }

    public static int sumArray(String[][] array) throws MyArraySizeException, MyArrayDataException {

        if (array.length != dimension)
            throw new MyArraySizeException("Опасное исключение! Текущий размер массива "+ array.length +"x"+ dimension +". Размерность массива должна быть " + dimension + "x" + dimension);

        int sum = 0;

        for(int i = 0; i < array.length; i++)
        {
            if (array[i].length != dimension)
                throw new MyArraySizeException("Опасное исключение! Текущий размер массива "+ array.length +"x"+ array[i].length +". Размерность всех внутренних массивов должна быть " + dimension + "x" + dimension);

            for(int j = 0; j < array[i].length; j++)
            {
                try
                {
                    int number = Integer.parseInt(array[i][j]);
                    sum += number;
                }
                catch (NumberFormatException ex)
                {
                    throw new MyArrayDataException(i, j, array[i][j]);
                }
            }
        }

        return sum;
    }

}
