package JavaLevel_2.lesson_3;

import java.util.*;

public class PhoneBook {

    private HashMap<String, HashSet<String>> hashMap = new HashMap<>(); // используем в качестве значения HashSet, чтобы не было дублей телефонов под одной фамилией

    public static void main(String[] args) {

        PhoneBook phoneBook = new PhoneBook();
        phoneBook.add("Петров", "455677");
        phoneBook.add("Сидоров", "888888");
        phoneBook.add("Морозов", "3438888");
        phoneBook.add("Петров", "788890");
        phoneBook.add("Петров", "123456");
        phoneBook.add("Петров", "123456"); // дубль не добавится

        System.out.println("Номера телефонов по фамилии Петров:");
        System.out.println(phoneBook.get("Петров"));
        System.out.println("\nНомера телефонов по фамилии Сидоров");
        System.out.println(phoneBook.get("Сидоров"));
        System.out.println("\nВывод всей телефонной книги:");
        phoneBook.printPhoneBook();
    }

    public void add(String key, String value) {

        if (hashMap.containsKey(key))
        {
            hashMap.get(key).add(value);
        }
        else
        {
            HashSet<String> hashSet = new HashSet<>();
            hashSet.add(value);
            hashMap.put(key, hashSet);
        }
    }

    public HashSet<String> get(String key) {
        return hashMap.get(key);
    }


    public void printPhoneBook() {
        System.out.println(hashMap);
    }

}
