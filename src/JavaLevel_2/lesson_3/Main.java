package JavaLevel_2.lesson_3;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        String[] array = {"honda", "toyota", "bmw",
                "audi", "mersedes", "lifan",
                "honda", "wolkswagen", "lada",
                "honda", "bentley", "toyota",
                "skoda", "lada", "subaru",
                "lexus", "honda", "suzuki"};

        uniqueWords(array);

    }

    //-- Найдем кол-во дублирующихся элементов и получим список униклаьных слов
    public static void uniqueWords(String[] array) {

        HashSet<String> hashSet = new HashSet<String>(); // для хранения массива уникальных элементов
        HashMap<String, Integer> hashMap = new HashMap<>(); // для хранения количества неуникальных элементов
        Boolean success;

        for(int i = 0; i < array.length; i++)
        {
            // заполняем hashSet элементами массива
            success = hashSet.add(array[i]); // Если элемент не уникален, вернет false

            // считаем количество повторений каждого слова
            if (success)
                hashMap.put(array[i], 1);
            else
                hashMap.put(array[i], (hashMap.get(array[i]) + 1));

        }

        System.out.println("Количество повторений каждого слова:");
        System.out.println(hashMap);
        System.out.println("\nСписок без дублей:");
        System.out.println(hashSet);
    }

}
