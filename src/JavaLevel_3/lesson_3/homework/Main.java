package JavaLevel_3.lesson_3.homework;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Scanner;

public class Main {

    public static int PAGE_SYMBOL = 1800;

    public static void main(String[] args) {
        task1();
        task2();
        task3();
    }

    //-- reading a file into a byte array and output it
    public static void task1() {
        File file = new File("files/1.txt");
        byte[] b = new byte[50];
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            for (int i = 0; i < b.length; i++) {
                out.write(i);
            }

            in = new FileInputStream(file);
            in.read(b);
            String str = new String(b);
            System.out.println(str);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //-- splice 5 files into 1
    public static void task2() {
        File[] files = createFiles(5, 100);
        ArrayList<InputStream> arrayList = new ArrayList<>();
        SequenceInputStream seq = null;
        FileOutputStream out = null;

        try {
            for (int i = 0; i < files.length; i++) {
                arrayList.add(new FileInputStream(files[i]));
            }

            Enumeration<InputStream> e = Collections.enumeration(arrayList);
            seq = new SequenceInputStream(e);
            out = new FileOutputStream("files/total.txt");
            int rb;
            while ((rb = seq.read()) != -1) {
                out.write(rb);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } finally {
            try {
                seq.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //-- read page file
    public static void task3() {

        int number_min = 97; // symbol "a"
        int number_max = 112; // sybol "p"
        int dec;
        char c;

        try {
            //-- creating file
            File file = new File("files/task3.txt");
            FileOutputStream out = new FileOutputStream(file);
            for (int i = 0; i < 10000; i++) {
                //Generate number
                dec = number_min + (int)(Math.random() * ((number_max - number_min) + 1));
                c = (char) dec;
                out.write(c);
            }

            byte[] b = new byte[PAGE_SYMBOL];
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            Scanner scanner = null;
            String pageString;
            int pageNumber;
            int pos;
            while (true) {
                scanner = new Scanner(System.in);
                if (scanner.hasNext()) {
                    pageNumber = scanner.nextInt();

                    if (pageNumber > 0) {
                        pos = pageNumber * PAGE_SYMBOL - PAGE_SYMBOL; // начальная позиция
                        raf.seek(pos);
                        raf.read(b); // читаем симовлы
                        pageString = new String(b);
                        System.out.println(pageString); // считанная страница
                    } else {
                        System.out.println("Число должно быть положительным!");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static File[] createFiles(int countFiles, int countBytes) {
        File[] files = new File[countFiles];
        FileOutputStream out = null;
        try {
            for (int i = 0; i < countFiles; i++) {
                files[i] = new File("files/" + i + ".txt");
                out = new FileOutputStream(files[i]);
                for (int j = 0; j < countBytes; j++) {
                    out.write(j);
                }
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return files;
    }

}
