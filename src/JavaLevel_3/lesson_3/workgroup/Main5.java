package JavaLevel_3.lesson_3.workgroup;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main5 {
    public static void main(String[] args) throws Exception{
        Book book = new Book(10);
        Student s = new Student("Morty", 10, book);
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("stud.ser"));
        oos.writeObject(s);
        oos.close();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("stud.ser"));
        Student s2 = (Student) ois.readObject();
        ois.close();
        s.info();
        s2.info();
        System.out.println(s.toString());
        System.out.println(s2.toString());
        System.out.println(s.book.toString());
        System.out.println(s2.book.toString());
    }
}
