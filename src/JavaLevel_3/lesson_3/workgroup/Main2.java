package JavaLevel_3.lesson_3.workgroup;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main2 {
    public static void main(String[] args){
        long t = System.currentTimeMillis();
       try (FileInputStream in = new FileInputStream("files/3.txt")){
           int x;
           byte[] barr = new byte[1024];
//           while((x = in.read()) != -1) { //-1 - это служебный символ, означающий что данных в потоке больше нет
////               System.out.print((char) x);
//           }
//           while((x = in.read()) != -1) { //-1 - это служебный символ, означающий что данных в потоке больше нет
//                          System.out.print((char) x); }
           String str;
           while(in.read(barr) > 0){
               str = new String(barr);
               System.out.print(str);
           }
           System.out.println("TIME: " + (System.currentTimeMillis() - t));
       }catch(FileNotFoundException e){

       }catch(IOException e){

       }
        try (FileOutputStream out = new FileOutputStream("files/2.txt")){
            for(int i = 0; i < 100; i++){
                out.write(65);
            }
        }catch(FileNotFoundException e){

        }catch(IOException e){

        }
    }
}
