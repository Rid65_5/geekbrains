package JavaLevel_3.lesson_3.workgroup;

import java.io.*;

public class Human implements Externalizable{
    int x;
    public Human(){
        System.out.println("Human Constructor Called!");
    }
    public Human(int x){
        this.x = x;
    }
    @Override
    public void writeExternal(ObjectOutput out) throws IOException{
    }
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException{
    }
}
