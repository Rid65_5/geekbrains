package JavaLevel_3.lesson_3.workgroup;

import java.io.*;

public class Main4 {
    public static void main(String[] args) throws Exception{
        PipedInputStream pin = new PipedInputStream();
        PipedOutputStream pos = new PipedOutputStream(pin);
        RandomAccessFile raf = new RandomAccessFile("files/2.txt", "rw");
        raf.seek(1000);
        FileInputStream in1 = new FileInputStream(new File("1.txt"));
        FileInputStream in2 = new FileInputStream(new File("files/2.txt"));
        SequenceInputStream sq = new SequenceInputStream(in1, in2);
    }
}
