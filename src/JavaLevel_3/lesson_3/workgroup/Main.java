package JavaLevel_3.lesson_3.workgroup;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws Exception{
        File file = new File("1.txt");
        System.out.println(file.exists());
        File dir = new File("files");
        System.out.println(Arrays.toString(dir.list()));
        String[] filtered = dir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name){
                return name.startsWith("2");
            }
        });
        System.out.println(Arrays.toString(filtered));
        File file2 = new File("files/asdf.askldjfhaldskf");
        System.out.println(file2.canExecute());
        File file3 = new File("files/5/");
        file3.mkdir();
        File file4 = new File("files/5/10/15/17");
        file4.mkdirs();
    }
}
