package JavaLevel_3.lesson_4.homework;

public class MFU {

    private Object monitorPrint = new Object();
    private Object monitorScan = new Object();

    public void print() {
        new Thread(() -> {
            synchronized (monitorPrint) {
                try {
                    System.out.println("Начало печати документа потоком " + Thread.currentThread().getName());
                    for (int i = 0; i < 5; i++) {
                        System.out.println(i);
                        Thread.sleep(50);
                    }
                    System.out.println("Конец печати документа потоком " + Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void scan() {
        new Thread(() -> {
            synchronized (monitorScan) {
                try {
                    System.out.println("Начало скана документа потоком " + Thread.currentThread().getName());
                    for (int i = 0; i < 5; i++) {
                        System.out.println(i);
                        Thread.sleep(50);
                    }
                    System.out.println("Конец скана документа потоком " + Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
