package JavaLevel_3.lesson_4.homework;

import java.io.*;

public class Main {

    public static Object monitor = new Object();
    public static Object monitor2 = new Object();
    public static char c = 'A';
    public static BufferedWriter out;
    public static int number = 0;

    public static void main(String[] args) {
        //task1();
        //task2();
        task3();
    }

    public static void task1() {
        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    synchronized (monitor) {
                        while (c != 'A') {
                            monitor.wait();
                        }
                        System.out.print(c);
                        c = 'B';
                        monitor.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    synchronized (monitor) {
                        while (c != 'B') {
                            monitor.wait();
                        }
                        System.out.print(c);
                        c = 'C';
                        monitor.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    synchronized (monitor) {
                        while (c != 'C') {
                            monitor.wait();
                        }
                        System.out.print(c);
                        c = 'A';
                        monitor.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public static void task2() {

        try {
            Main.out = new BufferedWriter(new FileWriter("files/threadFile.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 5; i++)
                    {
                        synchronized (monitor2) {
                            while (number != 0) {
                                monitor2.wait();
                            }
                            for (int j = 0; j < 10; j++) {
                                out.write("Thread " + Thread.currentThread().getName() + " write number " + j + "\n");
                                out.flush();
                                Thread.sleep(20);
                            }
                            number = 1;
                            monitor2.notifyAll();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 5; i++)
                    {
                        synchronized (monitor2) {
                            while (number != 1) {
                                monitor2.wait();
                            }
                            for (int j = 0; j < 10; j++) {
                                out.write("Thread " + Thread.currentThread().getName() + " write number " + j + "\n");
                                out.flush();
                                Thread.sleep(20);
                            }
                            number = 2;
                            monitor2.notifyAll();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread thread3 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 5; i++)
                    {
                        synchronized (monitor2) {
                            while (number != 2) {
                                monitor2.wait();
                            }
                            for (int j = 0; j < 10; j++) {
                                out.write("Thread " + Thread.currentThread().getName() + " write number " + j + "\n");
                                out.flush();
                                Thread.sleep(20);
                            }
                            number = 0;
                            monitor2.notifyAll();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();

    }

    public static void task3() {
        MFU mfu = new MFU();
        mfu.print();
        mfu.print();
        mfu.print();
        mfu.scan();
        mfu.scan();
        mfu.scan();
    }
}
