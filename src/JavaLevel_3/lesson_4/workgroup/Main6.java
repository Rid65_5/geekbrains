package JavaLevel_3.lesson_4.workgroup;

public class Main6 {
    public static Object monitor = new Object();
    public static char currentChar = 'A';
    public static void main(String[] args){
        new Thread(()-> {
            try{
                for(int i = 0; i < 10; i++){
                    synchronized(monitor){
                        while(currentChar != 'A'){
                            monitor.wait(); //монитор освобождается
                        }
                        System.out.print("A");
                        currentChar = 'B';
                        monitor.notifyAll();
                    }
                }
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }).start();
        new Thread(()-> {
            try{
                for(int i = 0; i < 10; i++){
                    synchronized(monitor){
                        while(currentChar != 'B'){
                            monitor.wait();
                        }
                        System.out.print("B");
                        currentChar = 'A';
                        monitor.notifyAll();
                    }
                }
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }).start();
    }
}
