package JavaLevel_3.lesson_4.workgroup;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main4 {
    public static void main(String[] args){
        ExecutorService service = Executors.newFixedThreadPool(5);
        for(int i = 0; i < 2; i++){
            final int w = i;
            service.execute(new Runnable() {
                @Override
                public void run(){
                    float x = 0.0f;
                    for(int j = 0; j < 1000000; j++){
                        if(Thread.currentThread().isInterrupted()) break;
                        System.out.println(j);
                        for(int k = 0; k < 1000000; k++){
                            x = (float) Math.cos(1000.0f) + (float) Math.cos(1000.f);
                        }
                    }
                }
            });
            try{
                Thread.sleep(4000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            service.shutdownNow();
        }
    }
}
