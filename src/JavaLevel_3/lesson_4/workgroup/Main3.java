package JavaLevel_3.lesson_4.workgroup;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main3 {
    public static void main(String[] args){
        ExecutorService service = Executors.newFixedThreadPool(5);
        for(int i = 0; i < 10; i++){
            final int w = i;
            service.execute(new Runnable() {
                @Override
                public void run(){
                    System.out.println(w + " - begin");
                    if(Thread.currentThread().isInterrupted()) System.out.println("Thread is Interrupted!");
                    try{
                        Thread.sleep(100 + (int) (3000*Math.random()));
                    }catch(InterruptedException e){
                    }
                    System.out.println(w + " - end");
                }
            });
        }
//        service.execute(new Runnable() {
//            @Override
//            public void run(){
//                System.out.println("test");
//            }
//        });
//        Executors.newCachedThreadPool();
        service.shutdown(); //перестает принимать задачи
        service.shutdownNow(); //выставляет всем потокам флаг isInterrupted == true
    }
}
