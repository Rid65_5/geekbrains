package JavaLevel_3.lesson_4.workgroup;

public class Main5 {
    public static Object lock = new Object();
    public static void main(String[] args){
        Main5 main5 = new Main5();
        new Thread(()-> main5.method1()).start();
        new Thread(()-> main5.method1()).start();
        new Thread(()-> main5.method1()).start();
    }
    public static void method1(){
        try{
            System.out.println("Non Synchronized Begin " + Thread.currentThread().getName());
            for(int i = 0; i < 5; i++){
                System.out.println(" . ");
                Thread.sleep(100);
            }
            System.out.println("Non Synchronized End " + Thread.currentThread().getName());
            synchronized(lock){
                System.out.println("Synchronized Begin " + Thread.currentThread().getName());
                for(int i = 0; i < 5; i++){
                    System.out.println(" % ");
                    Thread.sleep(200);
                }
                System.out.println("Synchronized end " + Thread.currentThread().getName());
            }
        }catch(InterruptedException e){
        }
    }
}
