package JavaLevel_3.lesson_4.workgroup;

public class Main2 {
    public static void main(String[] args){
        Thread timer = new Thread(()->{
            int time = 0;
            while(true){
                try{
                    Thread.sleep(1000);
                    time++;
                    System.out.println("Time: "  + time);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
        });
        timer.setDaemon(true);
        timer.start();
        System.out.println("Main - sleep");
        try{
            Thread.sleep(5000);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Main - end");
    }
}
