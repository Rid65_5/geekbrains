package JavaLevel_3.lesson_4.workgroup;

public class Main {
    public static void main(String[] args){
        Thread t1 = new ThreadClass();
        Thread t2 = new Thread(new RunnableInterface());
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run(){
                for(int i = 0; i < 10; i++){
                    System.out.println(i);
                    try{
                        Thread.sleep(500);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        });
        t1.start();
        t2.start();
        t3.start();
        System.out.println("Test");
        try{
            t1.join();
            System.out.println("T1 end");
            t2.join();
            System.out.println("T2 end");
            t3.join();
            System.out.println("T3 end");
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("Main End");
    }
}
