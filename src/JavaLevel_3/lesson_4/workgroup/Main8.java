package JavaLevel_3.lesson_4.workgroup;

public class Main8 {
    private static Integer n = new Integer(300);
    private static StringBuffer s = new StringBuffer("A");
    public static void main(String[] args){
        new Thread(new Runnable() {
            @Override
            public void run(){
                synchronized(s){
                    System.out.println("X");
//                    n++;
                    s.append("!");
                    try{
                        Thread.sleep(10000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run(){
                synchronized(s){
                    System.out.println("X");
//                    n++;
                    s.append("!");
                    try{
                        Thread.sleep(10000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run(){
                synchronized(s){
                    System.out.println("X");
//                    n++;
                    s.append("!");
                    try{
                        Thread.sleep(10000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

}
