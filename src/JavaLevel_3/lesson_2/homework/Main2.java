package JavaLevel_3.lesson_2.homework;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main2 implements Commands{

    public static void main(String[] args) {

        try {
            Main.connect();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            Main.disconnect();
            e.printStackTrace();
        }

        while (true)
        {
            Scanner in = new Scanner(System.in);

            if (in.hasNext())
            {
                String command = in.nextLine();

                if (command.startsWith(SYSTEM_SYMBOL))
                {
                    String[] arr = command.split(" ");

                    if (command.startsWith(GET_PRICE))
                    {
                        try {
                            String cost = Main.getCostByName(arr[1]);
                            if (cost == null)
                                System.out.println("Такого товара не найдено!");
                            else
                                System.out.println("Стоимость " + cost);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (command.startsWith(CHANGE_PRICE))
                    {
                        try {
                            Main.updateCost(arr[1], Double.parseDouble(arr[2]));
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (command.startsWith(PRICE_BETWEEN))
                    {
                        try {
                            HashMap<String, ArrayList<String>> hashMap = Main.getProductBetweenCost(Double.parseDouble(arr[1]), Double.parseDouble(arr[2]));
                            System.out.println("Товары между ценами: " + hashMap.get("titles").toString());
                            System.out.println("Цены на товары: " + hashMap.get("costs").toString());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    else System.out.println("Command not found!");
                }
            }
        }

    }

}
