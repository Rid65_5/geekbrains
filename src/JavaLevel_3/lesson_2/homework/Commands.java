package JavaLevel_3.lesson_2.homework;

public interface Commands {

    String SYSTEM_SYMBOL = "/";
    String GET_PRICE = "/price";
    String CHANGE_PRICE = "/change_price";
    String PRICE_BETWEEN = "/between_price";

}
