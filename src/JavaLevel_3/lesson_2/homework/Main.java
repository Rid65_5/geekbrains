package JavaLevel_3.lesson_2.homework;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    private static Connection connection;
    private static PreparedStatement ps;
    private static String tableName = "products";

    public static void main(String[] args) {
        try {
            connect();
            dropTable();
            createTable();
            createProducts();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            disconnect();
        }
    }

    public static void connect() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:my_db.db");
    }

    public static void disconnect() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createTable() throws SQLException {
        ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS products (id INTEGER PRIMARY KEY, prod_id INTEGER, title VARCHAR (300), cost REAL (15, 2));");
        ps.execute();
    }

    public static void dropTable() throws SQLException {
        ps = connection.prepareStatement("DROP TABLE products");
        ps.execute();
    }

    public static void createProducts() throws SQLException {
        connection.setAutoCommit(false);
        ps = connection.prepareStatement("INSERT INTO products (prod_id, title, cost) VALUES (?,?,?);");
        for (int i = 1; i <= 10000; i++) {
            ps.setInt(1, i);
            ps.setString(2, "product" + i);
            ps.setDouble(3, i*10);
            ps.executeUpdate();
        }
        connection.commit();
    }

    public static String getCostByName(String name) throws SQLException {
        ps = connection.prepareStatement("SELECT cost FROM " + tableName + " WHERE title = ?;");
        //ps.setString(1, tableName);
        ps.setString(1, name);
        ResultSet resultSet = ps.executeQuery();
        if (resultSet.next()) return resultSet.getString("cost");
        return null;
    }

    public static void updateCost(String title, double cost) throws SQLException {
        ps = connection.prepareStatement("UPDATE "+ tableName +" SET cost = ? WHERE title=?;");
        ps.setDouble(1, cost);
        ps.setString(2, title);
        ps.executeUpdate();
    }

    public static HashMap<String, ArrayList<String>> getProductBetweenCost(double cost1, double cost2) throws SQLException {
        ps = connection.prepareStatement("SELECT id, title, cost FROM "+ tableName + " WHERE cost BETWEEN ? AND ?;");
        ps.setDouble(1, cost1);
        ps.setDouble(2, cost2);
        ResultSet resultSet = ps.executeQuery();

        HashMap<String, ArrayList<String>> hashMap = new HashMap<>();
        hashMap.put("titles", new ArrayList<>());
        hashMap.put("costs", new ArrayList<>());

        while (resultSet.next()) {
            hashMap.get("titles").add(resultSet.getString("title"));
            hashMap.get("costs").add(resultSet.getString("cost"));
        }

        return hashMap;
    }
}
