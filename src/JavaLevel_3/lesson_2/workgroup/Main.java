//CREATE TABLE students (
//        id    INTEGER PRIMARY KEY AUTOINCREMENT,
//        name  TEXT,
//        score INTEGER
//        );

package JavaLevel_3.lesson_2.workgroup;

import java.sql.*;

public class Main {
    private static Connection connection;
    private static Statement statement;
    private static PreparedStatement ps;

    public static void main(String[] args) {
        try {
            connect();
            dropTable();
            createTable();
            preparedStatementData();
            getData();
//            preparedStatementBatch();
//            prepareData();
//            clearTable();
//            addData();
//            updateData();
//            statement.execute("INSERT INTO students (name, score) VALUES ('Rick1', 10);");
//            Savepoint sp1 = connection.setSavepoint(); //отключает автокоммит
//            statement.executeUpdate("INSERT INTO students (name, score) VALUES ('Rick2', 20);");
//            connection.rollback(sp1);
//            statement.executeUpdate("INSERT INTO students (name, score) VALUES ('Rick3', 30);");
//            connection.commit(); //commit уничтожает все savepoints

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            disconnect();
        }
    }

    public static void getData() throws SQLException {
        ResultSet resultSet = statement.executeQuery("SELECT id, name FROM students WHERE score > 50");
        while (resultSet.next()) System.out.println(resultSet.getInt(1) + " " + resultSet.getString("name"));
    }

    public static void preparedStatementBatch() throws SQLException {
        connection.setAutoCommit(false);
        long t = System.currentTimeMillis();
        ps = connection.prepareStatement("INSERT INTO students(name, score) VALUES (?,?);");
        for (int i = 0; i < 100000; i++) {
            ps.setString(1, "Rick" + i);
            ps.setInt(2, (i * 10));
            ps.addBatch();
        }
        ps.executeBatch();
        connection.commit();
        System.out.println("TIME: " + (System.currentTimeMillis() - t));
    }

    public static void preparedStatementData() throws SQLException {
        connection.setAutoCommit(false);
        ps = connection.prepareStatement("INSERT INTO students(name, score) VALUES (?,?);");
        for (int i = 0; i < 100; i++) {
            ps.setString(1, "Rick" + i);
            ps.setInt(2, (i * 10));
            ps.executeUpdate();
        }
        connection.commit();

    }

    public static void prepareData() throws SQLException {
        long t = System.currentTimeMillis();
        connection.setAutoCommit(false);
        for (int i = 0; i < 5000; i++) {
            statement.executeUpdate("INSERT INTO students(name, score) VALUES ('Rick" + i + "'," + i * 10 + ");");
        }
        connection.commit(); //запись в БД
        System.out.println("TIME: " + (System.currentTimeMillis() - t));
    }

    public static void createTable() throws SQLException {
        statement.execute("CREATE TABLE IF NOT EXISTS students ( id  INTEGER PRIMARY KEY AUTOINCREMENT, name  TEXT, score INTEGER);");
    }

    public static void clearTable() throws SQLException {
        statement.execute("DELETE FROM students");
    }

    public static void dropTable() throws SQLException {
        statement.execute("DROP TABLE students");
    }

    public static void addData() throws SQLException {
        for (int i = 0; i < 10; i++) {
            statement.executeUpdate("INSERT INTO students(name, score) VALUES ('Rick" + i + "'," + i * 10 + ");");
        }
    }

    public static void updateData() throws SQLException {
        statement.executeUpdate("UPDATE students SET score = 100 WHERE name='Rick4'");
    }

    public static void connect() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:jcpl2_db.db");
        statement = connection.createStatement();
    }

    public static void disconnect() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
