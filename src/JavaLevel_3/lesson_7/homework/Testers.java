package JavaLevel_3.lesson_7.homework;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;

public class Testers {

    public static void main(String[] args) {
        try {
            Testers.start(SomeTest.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void start(Class someTest) throws Exception {
        System.out.println("Tests start.");
        // Проверка и получение методов с анотациями @beforesuite и @aftersuite
        Method[] methods = someTest.getDeclaredMethods();
        int countBeforeSuite = 0;
        int countAfterSuite = 0;
        Method methodBeforeSuite = null;
        Method methodAfterSuite = null;
        ArrayList<Method> methodsList = new ArrayList<Method>();
        for (Method method : methods) {
            if (method.getAnnotation(BeforeSuite.class) != null) {
                methodBeforeSuite = method;
                countBeforeSuite++;
                if (countBeforeSuite > 1) {
                    System.out.println("Количество методов с аннотацией @BeforeSuite > 1.");
                    throw new RuntimeException();
                }
            } else if (method.getAnnotation(AfterSuite.class) != null) {
                methodAfterSuite = method;
                countAfterSuite++;
                if (countAfterSuite > 1) {
                    System.out.println("Количество методов с аннотацией @AfterSuite > 1.");
                    throw new RuntimeException();
                }
            } else if (method.getAnnotation(Test.class) != null) {
                methodsList.add(method);
            }
        }

        Constructor constr = someTest.getConstructor(); // Получили конструктор поумолчанию
        Object obj = constr.newInstance(); // создали объект Object
        someTest.cast(obj); // привели Object к объекту класса someTest

        // выполняем метод BeforeSuite
        if (methodBeforeSuite != null) {
            methodBeforeSuite.invoke(obj);
        }

        // сортировка методов с аннотацией @Test в порядке убывания приоритета
        Comparator<Method> comparator = new Comparator<Method>() {
            public int compare(Method o1, Method o2) {
                return o2.getAnnotation(Test.class).priority() - o1.getAnnotation(Test.class).priority();
            }
        };
        Collections.sort(methodsList, comparator);

        // Выполнение тестов
        for (Method method : methodsList) {
            System.out.print("Test Method " + method.getName() + ": ");
            method.invoke(obj);
        }

        // выполняем метод AfterSuite
        if (methodAfterSuite != null) {
            methodAfterSuite.invoke(obj);
        }

        System.out.println("Tests end.");
    }

}
