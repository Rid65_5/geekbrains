package JavaLevel_3.lesson_7.homework;

public class SomeTest {

    public int int1;
    public int int2;
    public String str1;
    public String str2;

    @BeforeSuite
    public void init() {
        System.out.println("BeforeSuite >>>");
        int1 = 5;
        int2 = 7;
        str1 = "Cat";
        str2 = "Dog";
    }

    @AfterSuite
    public void close() {
        System.out.println("AfterSuite >>>");
    }

    @Test(priority = 3)
    public void compare1() {
        System.out.println(int1 == int2);
    }

    @Test(priority = 7)
    public void compare2() {
        System.out.println(str1.equals(str2));
    }

    @Test(priority = 5)
    public void compare3() {
        System.out.println(int1 != int2);
    }
}
