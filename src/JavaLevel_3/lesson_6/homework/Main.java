package JavaLevel_3.lesson_6.homework;

import java.util.Arrays;
import java.util.List;

public class Main {

    public final int ELEMENT = 4;
    public final int ELEMENT1 = 1;

    public static void main(String[] args) {
        Main main = new Main();
        int[] arr = {4,4,4,4,1,1,1};
        int[] arr1 = main.task1(arr);
        boolean success = main.task2(arr);
        System.out.println(Arrays.toString(arr1));
        System.out.println(success);
    }

    public int[] task1(int[] arr) throws RuntimeException {

        if (arr.length == 0)
            return null;

        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == ELEMENT)
                index = i; //  определили индекс последней четверки
        }

        if (index == -1)
            throw new RuntimeException("В массиве нет элементов " + ELEMENT);

        int[] arr1 = new int[arr.length - index - 1];
        System.arraycopy(arr, index + 1, arr1, 0, arr.length - index - 1);

        return arr1;
    }

    public boolean task2(int[] arr) {
        boolean success1 = false;
        boolean success2 = false;

        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i] == ELEMENT)
                success1 = true;
            else if (arr[i] == ELEMENT1)
                success2 = true;
            else
                return false;
        }

        return success1 && success2;
    }

}
