package JavaLevel_3.lesson_6.homework.tests;

import JavaLevel_3.lesson_6.homework.Main;
import JavaLevel_3.lesson_6.homework.StudentsDB;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

public class Test3Param {

    public static int id = 3;

    @Test
    public void addRecord() {
        try {
            Assert.assertEquals(1, StudentsDB.addStudent("Test1", 200));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = SQLException.class)
    public void addRecordException() throws SQLException {
        StudentsDB.disconnect();
        StudentsDB.addStudent("Test1", 200);
    }

    @Test
    public void updateRecord() throws SQLException {
        Assert.assertEquals(1, StudentsDB.updateStudent(id, "Test1", 300));
    }

    @Test(expected = SQLException.class)
    public void updateRecordException() throws SQLException {
        StudentsDB.disconnect();
        Assert.assertEquals(1, StudentsDB.updateStudent(id, "Test1", 300));
    }

    @Test
    public void getRecord() throws SQLException {
        Assert.assertNotNull(StudentsDB.getRecordById(id));
    }

    @Before
    public void init() throws SQLException {
        StudentsDB.connect();
        StudentsDB.connection.setAutoCommit(false);
    }

    @After
    public void close() {
        try {
            StudentsDB.connection.rollback();
            StudentsDB.disconnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
