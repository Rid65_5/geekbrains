package JavaLevel_3.lesson_6.homework.tests;

import JavaLevel_3.lesson_6.homework.Main;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class Test1Param {

    public int[] arr; // исходный массив
    public int[] res; // результат метода
    public static Main main;

    public Test1Param(int[] numbers, int[] result) {
        this.arr = numbers;
        this.res = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        int[] arr1 = {1,4,5,6};
        int[] res1 = {5,6};
        int[] arr2 = {};
        int[] res2 = null;
        int[] arr3 = {1,4,5,6,4,8,9};
        int[] res3 = {8,9};
        int[] arr4 = {1,8,9};
        int[] res4 = null;
        return Arrays.asList(new Object[][]{
                {arr1, res1},
                {arr2, res2},
                {arr3, res3},
                {arr4, res4}
        });
    }

    @Test
    public void task1Test() {
        Assert.assertArrayEquals(res, main.task1(arr));
    }

    @Test(expected = RuntimeException.class)
    public void task1TestException() {
        int[] result = main.task1(arr);
    }

    @BeforeClass
    public static void init() {
        main = new Main();
    }

    @AfterClass
    public static void close() {

    }

}
