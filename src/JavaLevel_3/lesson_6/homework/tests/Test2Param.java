package JavaLevel_3.lesson_6.homework.tests;

import JavaLevel_3.lesson_6.homework.Main;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class Test2Param {

    public int[] arr; // исходный массив
    public boolean res; // результат метода
    public static Main main;

    public Test2Param(int[] numbers, boolean result) {
        this.arr = numbers;
        this.res = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        int[] arr1 = {1,4,1,1};
        boolean res1 = true;
        int[] arr2 = {};
        boolean res2 = false;
        int[] arr3 = {4,4,4};
        boolean res3 = false;
        int[] arr4 = {1,1,1,4,4,4};
        boolean res4 = true;
        return Arrays.asList(new Object[][]{
                {arr1, res1},
                {arr2, res2},
                {arr3, res3},
                {arr4, res4}
        });
    }

    @Test
    public void task2Test() {
        Assert.assertTrue(res == main.task2(arr));
    }

    @BeforeClass
    public static void init() {
        main = new Main();
    }

    @AfterClass
    public static void close() {

    }

}
