package JavaLevel_3.lesson_6.homework;

import java.sql.*;
import java.util.HashMap;

public class StudentsDB {

    public final static String tableName = "students";
    public static Connection connection;
    public static PreparedStatement preparedStatement;

    public static void main(String[] args) {
        try {
            return;
        } finally {
            System.out.println("sdfdsf");
        }
        //connect();
        //disconnect();
    }

    public static void connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:my_db.db");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void disconnect() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int addStudent(String surname, int score) throws SQLException {
        preparedStatement = connection.prepareStatement("INSERT INTO "+ tableName +" (surname, score) VALUES (?,?);");
        preparedStatement.setString(1, surname);
        preparedStatement.setInt(2, score);
        return preparedStatement.executeUpdate();
    }

    public static int updateStudent(int id, String surname, int score) throws SQLException {
        preparedStatement = connection.prepareStatement("UPDATE "+ tableName +" SET surname = ?, score = ?  WHERE id=?;");
        preparedStatement.setString(1, surname);
        preparedStatement.setInt(2, score);
        preparedStatement.setInt(3, id);
        return preparedStatement.executeUpdate();
    }

    public static HashMap<String, String> getRecordById(int id) throws SQLException {
        preparedStatement = connection.prepareStatement("SELECT * FROM " + tableName + " WHERE id = ?;");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        HashMap<String, String> hashMap = new HashMap<>();
        if (resultSet.next()) {
            hashMap.put("id", resultSet.getString("id"));
            hashMap.put("surname", resultSet.getString("surname"));
            hashMap.put("score", resultSet.getString("score"));
            return hashMap;
        }
        return null;
    }

    public static void delRecordBySurname(String surname) throws SQLException {
        preparedStatement = connection.prepareStatement("DELETE FROM " + tableName + " WHERE surname = ?;");
        preparedStatement.setString(1, surname);
        preparedStatement.executeUpdate();
    }


}
