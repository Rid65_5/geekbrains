package JavaLevel_3.lesson_1.workgroup;

//T - любой тип данных
//E - элемент коллекции
//K - ключ
//V - значение
//N - числовые значения

public class BoxGen<Q> {
    private Q obj;

    public Q getObj() {
        return obj;
    }

    public void setObj(Q obj) {
        this.obj = obj;
    }

    public BoxGen(Q obj) {
        this.obj = obj;
    }
}
