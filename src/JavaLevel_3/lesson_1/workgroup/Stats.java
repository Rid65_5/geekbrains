package JavaLevel_3.lesson_1.workgroup;

public class Stats<T extends Number> {

    private T[] nums;

    public Stats(T[] arr) {
        this.nums = arr;
    }

    public double avg() {
        double sum = 0.0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i].doubleValue();
        }
        return sum / nums.length;
    }

    public boolean sameAvg(Stats<?> obj) {
        return Math.abs(this.avg() - obj.avg()) < 0.0001;
    }

    public static void main(String[] args) {
        Integer[] inums = {1, 2, 3, 4, 5};
        Stats<Integer> intStats = new Stats<Integer>(inums);
        double v = intStats.avg();
        System.out.println("Average value: " + v);

        Double[] dnums = {1.23, 2.56, 3.3, 4.1, 5.8};
        Stats<Double> doubStats = new Stats<Double>(dnums);
        v = doubStats.avg();
        System.out.println("Average value: " + v);

        /*String[] snums = {"df", "sdfs"};
        Stats<String> sStats = new Stats<String>(snums);
        v = doubStats.avg();
        System.out.println("Average value: " + v);*/
    }

}
