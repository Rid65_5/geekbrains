package JavaLevel_3.lesson_1.workgroup;

import java.util.ArrayList;

public class Main2 {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        ArrayList<Integer> integers1 = new ArrayList<>();
        ArrayList<Float> floats = new ArrayList<>();
        ArrayList<Number> numbers = new ArrayList<>();
        ArrayList<String> strings = new ArrayList<>();
        copyElements(integers, numbers);
//        copyElements(numbers, integers);
//        copyElements(integers, floats);
        copyElements(floats, numbers);
//        copyElements(strings, numbers);
        copyElements(integers, integers1);

    }

    public static <T> void copyElements(ArrayList<? extends T> src, ArrayList<? super T> dst) {
        for (int i = 0; i < src.size(); i++) {
            dst.add(src.get(i));
        }
    }
}
