package JavaLevel_3.lesson_1.workgroup;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>(); //не поддерживают примитивные типы

        Box boxi1 = new Box(50);
        Box boxi2 = new Box(20);
        //500 lines of code

        boxi1.setObj("Hello, World!");

        //500 lines of code
        if (boxi1.getObj() instanceof Integer && boxi2.getObj() instanceof Integer) {
            int result = (Integer) boxi1.getObj() + (Integer) boxi2.getObj();
            System.out.println(result);
        }

        BoxGen<Integer> intBox1 = new BoxGen<>(3);
        BoxGen<Integer> intBox2 = new BoxGen<>(5);
//        intBox1.setObj("Hello!");
        BoxGen<String> strBox = new BoxGen<>("Hello");
        BoxGen<String> strBox2 = new BoxGen<>(" Java!");

        int result2 = intBox1.getObj() + intBox2.getObj();
        System.out.println(result2);
        String result3 = strBox.getObj() + strBox2.getObj();
        System.out.println(result3);

        NumberBox<Integer> integerNumberBox = new NumberBox<>(new Integer[]{1, 2, 3, 4, 5,});
        NumberBox<Double> doubleNumberBox = new NumberBox<>(new Double[]{1., 2., 3., 4., 5.});
        System.out.println(integerNumberBox.average());
        System.out.println(doubleNumberBox.average());
        System.out.println(integerNumberBox.compare(doubleNumberBox));

        ArrayList arl = new ArrayList(); //сырой тип
        arl.add("Java");
        arl.add(1);
        arl.add(new BoxGen<Integer>(5));
        compare(5, 6);

//        ArrayList<Number> a = new ArrayList<Integer>();
    }

    public static <T extends Comparable<T>> int compare(T obj1, T obj2) {
        return obj1.compareTo(obj2);
    }
}
