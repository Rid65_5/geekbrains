package JavaLevel_3.lesson_1.workgroup;

public class NumberBox<N extends Number> { //Number - parent class for Integer, Float, Double...
    private N[] numbers;
//    static N staticVar; нельзя создать static обобщенного типа

    public NumberBox(N[] numbers) {
        this.numbers = numbers;
    }

    public double average() {
        double d = 0.;
        for (N number : numbers) {
            d += number.doubleValue();
        }
        return d / this.numbers.length;
    }

    public boolean compare(NumberBox<?> anotherBox) {
        //0.1
        //0.1000000000000001
//        return this.average() == anotherBox.average();
        return Math.abs(this.average() - anotherBox.average()) < 0.0001f;
    }

}
