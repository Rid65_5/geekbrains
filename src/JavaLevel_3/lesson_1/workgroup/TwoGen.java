package JavaLevel_3.lesson_1.workgroup;

public class TwoGen<T, V> {

    private T obj1;
    private V obj2;

    public static void main(String[] args) {
        TwoGen<Integer, String> twoGen = new TwoGen<Integer, String>(555, "Hello");
        twoGen.showTypes();
        int intVal = twoGen.getObj1();
        String strVal = twoGen.getObj2();
        System.out.println(intVal);
        System.out.println(strVal);
    }

    public TwoGen(T obj1, V obj2) {
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    public void showTypes() {
        System.out.println("Type T: " + obj1.getClass().getName());
        System.out.println("Type V: " + obj2.getClass().getName());
    }

    public T getObj1() {
        return obj1;
    }

    public V getObj2() {
        return obj2;
    }
}
