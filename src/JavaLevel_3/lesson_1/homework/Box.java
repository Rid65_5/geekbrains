package JavaLevel_3.lesson_1.homework;

import java.util.ArrayList;
import java.util.Collections;

public class Box<T extends Fruit> {

    private ArrayList<T> box;

    public Box() {
        box = new ArrayList<>();
    }

    public Box(T[] arrFruit) {
        box = new ArrayList<>();
        Collections.addAll(box, arrFruit);
    }

    public ArrayList<T> getBox() {
        return box;
    }

    public int getCount() {
        return box.size();
    }

    public void addFruit(T fruit) {
        box.add(fruit);
    }

    public void addArrayFruit(T[] arrFruit) {
        ArrayList<T> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, arrFruit);
        box.addAll(arrayList);
    }

    public float getWeight() {
        float weight = 0.0f;
        for (int i = 0; i < box.size(); i++) {
            weight += box.get(i).getWeight();
        }
        return weight;
    }

    public boolean compare(Box<?> anotherBox) {
        return Math.abs(this.getWeight() - anotherBox.getWeight()) < 0.0001f;
    }

    public void moveToAnotherBox(Box<T> anotherBox) {

        anotherBox.getBox().addAll(this.box);

        /*for (T elem : box) {
            anotherBox.addFruit(elem);
        }*/
        box.clear();
    }
}
