package JavaLevel_3.lesson_1.homework;

public interface FruitParameters {

    float WEIGHT_APPLE = 1.0f;
    float WEIGHT_ORANGE = 1.5f;

}
