package JavaLevel_3.lesson_1.homework;

public abstract class Fruit implements FruitParameters {

    public float weight;

    public float getWeight() {
        return weight;
    }

}
