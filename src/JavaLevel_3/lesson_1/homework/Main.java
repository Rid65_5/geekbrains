package JavaLevel_3.lesson_1.homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {

        String[] strings = {"qwe", "asd", "zxc"};
        System.out.println("Исходный массив: " + Arrays.toString(strings));
        swap(strings, 0, 2);
        System.out.println("Поменяли местами элементы: " + Arrays.toString(strings));
        ArrayList arrayList = convertToArrayList(strings);
        System.out.println("Преобразовали в ArrayList: " + arrayList.toString());

        Apple[] apples = {new Apple(), new Apple(), new Apple()};
        Orange[] oranges = {new Orange(), new Orange(), new Orange()};
        Box<Apple> appleBox = new Box<>(apples);
        Box<Apple> appleBox1 = new Box<>(apples);
        Box<Orange> orangeBox = new Box<>(oranges);
        Box<Orange> orangeBox1 = new Box<>(oranges);

        System.out.println("Вес коробки с яблоками: " + appleBox.getWeight());
        System.out.println("Вес коробки с апельсинами: " + orangeBox.getWeight());
        System.out.println("Сравнение веса коробки яблок и апельсинов: " + appleBox.compare(orangeBox));
        System.out.println("Сравнение веса коробки яблок и яблок: " + appleBox.compare(appleBox1));
        System.out.println("Кол-во апельсинов в 1 коробке: " + orangeBox.getCount());
        System.out.println("Кол-во апельсинов во 2 коробке: " + orangeBox1.getCount());
        orangeBox.moveToAnotherBox(orangeBox1);
        System.out.println("Переложили апельсины из 1 коробки во 2.");
        System.out.println("Кол-во апельсинов в 1 коробке: " + orangeBox.getCount());
        System.out.println("Кол-во апельсинов во 2 коробке: " + orangeBox1.getCount());
    }

    // Method swaps elements of the array
    public static <T> void swap(T[] arr, int index1, int index2) {

        if (arr.length > index1 && arr.length > index2) {
            T a = arr[index1];
            arr[index1] = arr[index2];
            arr[index2] = a;
        }

    }

    // Convert Array to ArrayList
    public static <T> ArrayList<T> convertToArrayList(T[] arr) {
        ArrayList<T> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, arr);
        return arrayList;
    }

}
