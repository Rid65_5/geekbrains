package JavaLevel_3.lesson_5.homework;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.ReentrantLock;

public class Car implements Runnable {

    public static ReentrantLock lock = new ReentrantLock();
    private static int CARS_COUNT;
    static {
        CARS_COUNT = 0;
    }
    private Race race;
    private int speed;
    private String name;
    public String getName() {
        return name;
    }
    public int getSpeed() {
        return speed;
    }
    public Car(Race race, int speed) {
        this.race = race;
        this.speed = speed;
        CARS_COUNT++;
        this.name = "Участник #" + CARS_COUNT;
    }
    @Override
    public void run() {
        try {
            System.out.println(this.name + " готовится");
            Thread.sleep(500 + (int)(Math.random() * 800)); // Время подготовки машины
            System.out.println(this.name + " готов");

            MainClass.countDownLatchStart.countDown(); // уменьшаем счетчик
            MainClass.countDownLatchStart.await(); // Ожидаем обнуления счетчика, приостанавливаем поток
        } catch (Exception e) {
            e.printStackTrace();
        }


        // обход каждой стадии трассы
        for (int i = 0; i < race.getStages().size(); i++) {
            race.getStages().get(i).go(this);
        }

        if (lock.tryLock()) {
            System.out.println(this.name + " WIN!!!");
        }

        MainClass.countDownLatchFinish.countDown();
    }
}
