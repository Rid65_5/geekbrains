package JavaLevel_3.lesson_5.workgroup;

import java.util.concurrent.*;

public class Main6 {
    public static void main(String[] args){
        final int CLIENTS_COUNT = 1000;
        CountDownLatch latch = new CountDownLatch(CLIENTS_COUNT);
        long t = System.currentTimeMillis();
        for(int i = 0; i < CLIENTS_COUNT; i++){
            final int w = i;
            new Thread(new Runnable() {
                @Override
                public void run(){
//                    System.out.println("Thread: " + w);
                    latch.countDown();
                }
            }).start();
        }
        try{
            latch.await();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("TIME: " + (System.currentTimeMillis() - t));
        ExecutorService service = Executors.newFixedThreadPool(4);
        CyclicBarrier cyclicBarrier = new CyclicBarrier(4);
        for(int i = 0; i < 8; i++){
            service.execute(new Runnable() {
                @Override
                public void run(){
                    try{
                        System.out.println(Thread.currentThread().getName() + " is preparing");
                        Thread.sleep(1000 + (int)(Math.random() * 5000));
                        System.out.println(Thread.currentThread().getName() + " is prepared");
                        cyclicBarrier.await();
                        System.out.println(Thread.currentThread().getName() + " is running");
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }catch(BrokenBarrierException e){
                        e.printStackTrace();
                    }
                }
            });
        }
        service.shutdown();
    }



}
