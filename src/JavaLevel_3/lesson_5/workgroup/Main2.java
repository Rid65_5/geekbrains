package JavaLevel_3.lesson_5.workgroup;

import java.util.concurrent.*;

public class Main2 {
    public static void main(String[] args){
        ExecutorService service = Executors.newFixedThreadPool(5);
//        service.submit();
        Future<String> future = service.submit(new Callable<String>() {
            @Override
            public String call() throws Exception{
                Thread.sleep(4000);
//                int x = 10/0;
                return "A";
            }
        });
        try{
            System.out.println(future.get()); //ставит поток в режим ожидания, пока не будут получены данные
        }catch(InterruptedException e){
            e.printStackTrace();
        }catch(ExecutionException e){
            e.printStackTrace();
        }
        System.out.println("Main-end");
        service.shutdown();
    }
}
