package JavaLevel_3.lesson_5.workgroup;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Main7 {
    public static void main(String[] args){
        ExecutorService service = Executors.newFixedThreadPool(5);
        Lock lock = new ReentrantLock();
        ReentrantReadWriteLock lock1 = new ReentrantReadWriteLock();
        new Thread(new Runnable() {
            @Override
            public void run(){
//                lock.lock(); //переходим в режим ожидания
                if(lock.tryLock()){
                    try{
                        System.out.println("1");
                        Thread.sleep(1000);
                        lock.unlock(); //освободить замок
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run(){
                try{
                    if(lock.tryLock(800, TimeUnit.MILLISECONDS)){
                        System.out.println("2");
                        Thread.sleep(1000);
                        lock.unlock();
                    }
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
