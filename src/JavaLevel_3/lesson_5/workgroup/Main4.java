package JavaLevel_3.lesson_5.workgroup;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class Main4 {
    public static void main(String[] args){
        CopyOnWriteArrayList arrayList = new CopyOnWriteArrayList(); //для чтения
        HashMap<String, String> hm = new HashMap<>();
        Map<String, String> sm = Collections.synchronizedMap(hm); //два потока одновременно не монут читать и писать, полная синхронизация
        Map<String, String> sm2 = new ConcurrentHashMap<>();//позволяет одновременную работу с HashMap
    }
}
