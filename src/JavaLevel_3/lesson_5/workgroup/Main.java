package JavaLevel_3.lesson_5.workgroup;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class Main {
    public static void main(String[] args){
        ThreadFactory tf1 = new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r){
                return new Thread(r);
            }
        };
        ThreadFactory tf2 = new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r){
                Thread temp = new Thread(r);
                temp.setPriority(5);
                temp.setDaemon(true);
                return temp;
            }
        };
        ThreadFactory tf3 = new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r){
                Thread t1 = new Thread(r);
                System.out.println("Thread created!");
                return t1;
            }
        };

        ExecutorService service = Executors.newFixedThreadPool(5, tf3);
        for(int i = 0; i < 10; i++){
            service.execute(new Runnable() {
                @Override
                public void run(){
                }
            });
        }
        service.shutdown();
    }
}
