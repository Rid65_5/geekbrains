package JavaLevel_3.lesson_5.workgroup;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class Main5 {
    public static void main(String[] args){
        ExecutorService service = Executors.newFixedThreadPool(8);
        Semaphore semaphore = new Semaphore(4);
        for(int i = 0; i < 8; i++){
            service.execute(new Runnable() {
                @Override
                public void run(){
                    try{
                        semaphore.acquire(); //захватить
                        System.out.println("1");
                        Thread.sleep(2000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }finally{
                        semaphore.release();
                    }
                }
            });
        }
        service.shutdown();
    }

}
